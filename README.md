# Description
Fun with Ajax for Mustache and JSON data.

## Please visit
You can see [the lesson at gitlab.io](https://artavia.gitlab.io/ajax-for-mustache-and-json/ "the lesson at gitlab") and decide if this is something for you to play around with at a later date.

## What it has
It makes minimal Bootstrap css, asynchronous AJAX calls to each JSON data and one Mustache template but it is capable of receiving multiple quantities of templates if you choose. Each the data and the template reside in their own respective document independent of the main javascript file. 

## Unanticipated complications 
Unfortunately, I had some trouble with testing in my local environment on mobile devices. So depending on my findings during deployment, I may have to double&#45;back and insert some Babel/cli functionality into this project if it becomes necessary, then, recompile and edit the html in order to support **older mobile devices** due to the **ES5 Object.create() method**. 

## It has been a long time&hellip;
I have not played with templates in a long time. This is because libraries such as **React** and **Angular2+** encourage a shift from template&#45;based layouts to data&#45;driven components. Plus, the ES6 spec has been coming into its own more with features such as **module imports and exports** and **template literals** for starters. So, I felt it was time to address this long ignored piece of &quot;low-hanging fruit.&quot; 

## What is next
In no particular order and after getting acquainted first with each **Angular 6** and likely **AWS**, I would like to revisit **Knockout, EmberJS and Backbone**.

## Comment on project history
This item was originally constructed somewhere between February 27, 2014 to March 9 2014. Then, on April 2, 2014 (or thereabouts), I began to look at helper functions according to my own records.

## My name is Luis
You can call me don Lucho &amp; I hope you have fun during the rest of your day building something cool to share with the rest of the world!