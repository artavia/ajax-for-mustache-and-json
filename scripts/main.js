( function(){

  let mustacherequest = false;
  let dataFulfilled = null;
  let asyncMustacheTemplate = '';
  let templateName = "pirates";
  let myDataSourceName = `data-${templateName}`;

  // const str_host = window.location.host; // ~ 127.0.0.1:8080
  const str_href = window.location.href;
  const ar_new_href = str_href.split('/');
  const ar_hrefLength = ar_new_href.length;

  const displayOut = document.querySelector("#displayOut");
  
  const loadFunc = () => {

    let config = {};
    config.dataFulfilled = dataFulfilled; 
    config.dataSourceName = myDataSourceName;

    init( config );
  };

  const init = ( pm ) => { // console.log( "pm", pm );
    setupTemplates(pm.dataFulfilled);
    fetchTheJsonData( pm.dataSourceName );
    cancelLink();
    // urlOps();
  };

  const urlOps = () => { 
    // console.log( "str_host" , str_host ); // 127.0.0.1:8080
    console.log( "str_href" , str_href ); 
    console.log( "ar_new_href" , ar_new_href ); 
    console.log( "ar_hrefLength" , ar_hrefLength ); 
  };

  const cancelLink = () => {
    document.querySelector("#figurehead").addEventListener( "click", (evt) => {
      evt.preventDefault();
    }, false )
  };

  const setupTemplates = ( fulfilledBool ) => {
    
    extractPromiseObjects( fulfilledBool );

  };

  const extractPromiseObjects = ( pmFulfill ) => {
    if( pmFulfill === null ){
      pmFulfill = templateGrabber( templateName );
    }
    // console.log( "pmFulfill" , pmFulfill );
  };

  const templateGrabber = (templateName) => { 
    
    let livemustacherequest = mustacherequest; 
    livemustacherequest = new XMLHttpRequest();

    if( !!livemustacherequest ){
      livemustacherequest.onreadystatechange = compileToMustacheResponses; 
      
      // const url = `../templates/${templateName}.mustache`; 
      let url;

      if( ar_hrefLength === 5 ){
        url = `templates/${templateName}.mustache`; 
      }
      else
      if( ar_hrefLength === 4 ){
        url = `../templates/${templateName}.mustache`; 
      }

      livemustacherequest.overrideMimeType( 'text/x-tmpl-mustache' );
      livemustacherequest.open( "GET" , url, true ); 
      livemustacherequest.onload = neutralizeXHR;
      livemustacherequest.send( null ); 
    }
    else {
      alert("An XHR could not be requested."); 
    }

    return livemustacherequest;
  };

  const compileToMustacheResponses = (xhr) => {
  // function compileToMustacheResponses() {

    // 0 uninitialized - Has not started loading yet
    // 1 loading - The data is loading.
    // 2 loaded - The data has been loaded
    // 3 interactive - A part of the data is available and the user can interact with it
    // 4 complete - Fully loaded... Initialization is ready. 
    
    if( (xhr.target.readyState === 3) && (xhr.target.status === 200) ){ 
      var ifBonanza = xhr.target.responseURL.indexOf( templateName ) > -1;
      if( ifBonanza === true ){
        asyncMustacheTemplate = xhr.target.response;
        
        return asyncMustacheTemplate;
      }
    }
  };

  const neutralizeXHR = () => { 
    mustacherequest = null;
  };

  const fetchTheJsonData = ( name ) => {
    
    // let url = `../data/${name}.json`;

    let url;
    
    if( ar_hrefLength === 5 ){
      url = `data/${name}.json`;
    }
    else
    if( ar_hrefLength === 4 ){
      url = `../data/${name}.json`;
    }
    
    const request = new XMLHttpRequest();
    request.open("GET", url, true );
    request.onload = () => {
      if( request.status >= 200 && request.status < 400 ){
        
        const data = JSON.parse(request.response);
        createHTML(data);

      }
      else {
        displayOut.innerHTML = `<p>Connected to server but there was a problem: ${request.statusText}. </p>`;
        console.log(request);
      }
    };
    
    request.onerror = () => {
      displayOut.innerHTML = "<p>There has been a problem. </p>";
    };

    request.send();
  };

  const createHTML = ( parsedResults ) => { 

    // console.log( "parsedResults" , parsedResults );
    // console.log( "asyncMustacheTemplate" , asyncMustacheTemplate );

    let helper = {
      fullName: function () {
        return `${this.crewMember.firstName} ${this.crewMember.lastName}`;
      }
      , strong: function(){
        return function( text, render ){
          return `<strong>${render(text)}</strong>`;
        }
      }
      , JOLLYROGER: function() {

        // const string = `<img src=${this.jollyRoger.url} height=\"48\" alt="Picture of one of ${this.epithet}\'s flying Jolly Rogers." />`;

        let string;

        if( ar_hrefLength === 5 ){
          
          let href = this.jollyRoger.url;
          href = href.replace("../", "");
          string = `<img src=${href} height=\"48\" alt="Picture of one of ${this.epithet}\'s flying Jolly Rogers." />`;

        }
        else 
        if( ar_hrefLength === 4 ){
          string = `<img src=${this.jollyRoger.url} height=\"48\" alt="Picture of one of ${this.epithet}\'s flying Jolly Rogers." />`;
        }

        return string;
      }
      , IF_crew: function(){
        const string = `I am the crew ${this.occupation} and I am the ${this.th_Member} member to have come aboard.`; 

        return string;
      }
      , allyDeck: function() {
        const string = `I am a strategic ally to the Mugiwara.`;
        return string;
      }
      , normalJob: function(){
        
        const string = `My normal job is being the ${this.occupation}. I am the ${this.club_Member} member of the Mugiwara.`;

        return string;
      }
      , bounty_Description: function() {
        
        const string = `You probably know me as <em>${this.epithet}</em> and I carry a bounty of <strong>${this.bounty}</strong> Beri.`;
        
        return string;
      }
      , SAFER_url: function() { 
        const string = `Visit the wikia and see a <a href="${this.blog}" target=\"_blank\">${this.epithet}\'s bio</a> exclusively today.`;
        return string;
      }
      , UNL_deck: function() { 
        const string = `Together with the Mugiwara I have helped defeat powerful enemies and have come to know honorable, powerful friends in the Grand Line. Go see <a href=\"${this.blog}\" target=\"_blank\">${this.epithet}\'s personal bio</a> at the wiki site today.`;

        return string;
      }
    };

    let copy = Object.assign({}, parsedResults, helper );
    const rendered = Mustache.render( asyncMustacheTemplate, copy ); // console.log( "rendered", rendered );
    
    displayOut.insertAdjacentHTML("beforeend" , rendered ); // displayOut.innerHTML = rendered; 
  };

  window.onload = loadFunc;  

} )();