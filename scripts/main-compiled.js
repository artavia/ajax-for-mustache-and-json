"use strict";

(function () {
  var mustacherequest = false;
  var dataFulfilled = null;
  var asyncMustacheTemplate = '';
  var templateName = "pirates";
  var myDataSourceName = "data-".concat(templateName); // const str_host = window.location.host; // ~ 127.0.0.1:8080

  var str_href = window.location.href;
  var ar_new_href = str_href.split('/');
  var ar_hrefLength = ar_new_href.length;
  var displayOut = document.querySelector("#displayOut");

  var loadFunc = function loadFunc() {
    var config = {};
    config.dataFulfilled = dataFulfilled;
    config.dataSourceName = myDataSourceName;
    init(config);
  };

  var init = function init(pm) {
    // console.log( "pm", pm );
    setupTemplates(pm.dataFulfilled);
    fetchTheJsonData(pm.dataSourceName);
    cancelLink(); // urlOps();
  };

  var urlOps = function urlOps() {
    // console.log( "str_host" , str_host ); // 127.0.0.1:8080
    console.log("str_href", str_href);
    console.log("ar_new_href", ar_new_href);
    console.log("ar_hrefLength", ar_hrefLength);
  };

  var cancelLink = function cancelLink() {
    document.querySelector("#figurehead").addEventListener("click", function (evt) {
      evt.preventDefault();
    }, false);
  };

  var setupTemplates = function setupTemplates(fulfilledBool) {
    extractPromiseObjects(fulfilledBool);
  };

  var extractPromiseObjects = function extractPromiseObjects(pmFulfill) {
    if (pmFulfill === null) {
      pmFulfill = templateGrabber(templateName);
    } // console.log( "pmFulfill" , pmFulfill );

  };

  var templateGrabber = function templateGrabber(templateName) {
    var livemustacherequest = mustacherequest;
    livemustacherequest = new XMLHttpRequest();

    if (!!livemustacherequest) {
      livemustacherequest.onreadystatechange = compileToMustacheResponses; // const url = `../templates/${templateName}.mustache`; 

      var url;

      if (ar_hrefLength === 5) {
        url = "templates/".concat(templateName, ".mustache");
      } else if (ar_hrefLength === 4) {
        url = "../templates/".concat(templateName, ".mustache");
      }

      livemustacherequest.overrideMimeType('text/x-tmpl-mustache');
      livemustacherequest.open("GET", url, true);
      livemustacherequest.onload = neutralizeXHR;
      livemustacherequest.send(null);
    } else {
      alert("An XHR could not be requested.");
    }

    return livemustacherequest;
  };

  var compileToMustacheResponses = function compileToMustacheResponses(xhr) {
    // function compileToMustacheResponses() {
    // 0 uninitialized - Has not started loading yet
    // 1 loading - The data is loading.
    // 2 loaded - The data has been loaded
    // 3 interactive - A part of the data is available and the user can interact with it
    // 4 complete - Fully loaded... Initialization is ready. 
    if (xhr.target.readyState === 3 && xhr.target.status === 200) {
      var ifBonanza = xhr.target.responseURL.indexOf(templateName) > -1;

      if (ifBonanza === true) {
        asyncMustacheTemplate = xhr.target.response;
        return asyncMustacheTemplate;
      }
    }
  };

  var neutralizeXHR = function neutralizeXHR() {
    mustacherequest = null;
  };

  var fetchTheJsonData = function fetchTheJsonData(name) {
    // let url = `../data/${name}.json`;
    var url;

    if (ar_hrefLength === 5) {
      url = "data/".concat(name, ".json");
    } else if (ar_hrefLength === 4) {
      url = "../data/".concat(name, ".json");
    }

    var request = new XMLHttpRequest();
    request.open("GET", url, true);

    request.onload = function () {
      if (request.status >= 200 && request.status < 400) {
        var data = JSON.parse(request.response);
        createHTML(data);
      } else {
        displayOut.innerHTML = "<p>Connected to server but there was a problem: ".concat(request.statusText, ". </p>");
        console.log(request);
      }
    };

    request.onerror = function () {
      displayOut.innerHTML = "<p>There has been a problem. </p>";
    };

    request.send();
  };

  var createHTML = function createHTML(parsedResults) {
    // console.log( "parsedResults" , parsedResults );
    // console.log( "asyncMustacheTemplate" , asyncMustacheTemplate );
    var helper = {
      fullName: function fullName() {
        return "".concat(this.crewMember.firstName, " ").concat(this.crewMember.lastName);
      },
      strong: function strong() {
        return function (text, render) {
          return "<strong>".concat(render(text), "</strong>");
        };
      },
      JOLLYROGER: function JOLLYROGER() {
        // const string = `<img src=${this.jollyRoger.url} height=\"48\" alt="Picture of one of ${this.epithet}\'s flying Jolly Rogers." />`;
        var string;

        if (ar_hrefLength === 5) {
          var href = this.jollyRoger.url;
          href = href.replace("../", "");
          string = "<img src=".concat(href, " height=\"48\" alt=\"Picture of one of ").concat(this.epithet, "'s flying Jolly Rogers.\" />");
        } else if (ar_hrefLength === 4) {
          string = "<img src=".concat(this.jollyRoger.url, " height=\"48\" alt=\"Picture of one of ").concat(this.epithet, "'s flying Jolly Rogers.\" />");
        }

        return string;
      },
      IF_crew: function IF_crew() {
        var string = "I am the crew ".concat(this.occupation, " and I am the ").concat(this.th_Member, " member to have come aboard.");
        return string;
      },
      allyDeck: function allyDeck() {
        var string = "I am a strategic ally to the Mugiwara.";
        return string;
      },
      normalJob: function normalJob() {
        var string = "My normal job is being the ".concat(this.occupation, ". I am the ").concat(this.club_Member, " member of the Mugiwara.");
        return string;
      },
      bounty_Description: function bounty_Description() {
        var string = "You probably know me as <em>".concat(this.epithet, "</em> and I carry a bounty of <strong>").concat(this.bounty, "</strong> Beri.");
        return string;
      },
      SAFER_url: function SAFER_url() {
        var string = "Visit the wikia and see a <a href=\"".concat(this.blog, "\" target=\"_blank\">").concat(this.epithet, "'s bio</a> exclusively today.");
        return string;
      },
      UNL_deck: function UNL_deck() {
        var string = "Together with the Mugiwara I have helped defeat powerful enemies and have come to know honorable, powerful friends in the Grand Line. Go see <a href=\"".concat(this.blog, "\" target=\"_blank\">").concat(this.epithet, "'s personal bio</a> at the wiki site today.");
        return string;
      }
    };
    var copy = Object.assign({}, parsedResults, helper);
    var rendered = Mustache.render(asyncMustacheTemplate, copy); // console.log( "rendered", rendered );

    displayOut.insertAdjacentHTML("beforeend", rendered); // displayOut.innerHTML = rendered; 
  };

  window.onload = loadFunc;
})();
